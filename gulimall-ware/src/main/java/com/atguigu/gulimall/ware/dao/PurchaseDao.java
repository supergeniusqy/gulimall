package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author qiuyang
 * @email 862289485@qq.com
 * @date 2021-10-14 15:59:47
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
