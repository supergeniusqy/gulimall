package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author qiuyang
 * @email 862289485@qq.com
 * @date 2021-10-14 15:56:42
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
